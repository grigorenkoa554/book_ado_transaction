﻿using Book_ADO_Transaction.Repository;
using System;
using System.ComponentModel.DataAnnotations;

namespace Book_ADO_Transaction
{
    class Program
    {
        static void Main(string[] args)
        {
            var addressRepository = new AddressRepository();
            //addressRepository.Add(new Address { City = "Homel", Street = "Hataevicha 53", UserId = null });
            //addressRepository.Add(new Address { City = "Homel", Street = "Golovackogo 136", UserId = 3 });
            //addressRepository.Add(new Address { City = "Homel", Street = "Sosnovaya 20", UserId = 4 });
            foreach (var item in addressRepository.GetAll())
            {
                Console.WriteLine($"{item.Id}) City: {item.City}, Street: {item.Street}, UserId: {item.UserId}");
            }
            var userRepository = new UserRepository();
            //userRepository.ChangeAddress(4, 3);
            var userId = 3;
            var count = userRepository.GetCountAppartmentByUserId(userId);
            Console.WriteLine($"Count of userId: {userId} - is {count}.");

            var age = 88;
            var countOfAge = userRepository.GetCountUserByAge(age);
            Console.WriteLine($"Age = {age}, count of this result = {countOfAge}");
        }
    }
}
