﻿SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Adress](
	[City] [nvarchar](50) NULL,
	[Street] [nvarchar](50) NULL,
	[UserId] [int] NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_Adress] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Adress]  WITH CHECK ADD  CONSTRAINT [FK_Adress_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([ID])
GO
-------------------------------------------------
ALTER TABLE [dbo].[Adress] CHECK CONSTRAINT [FK_Adress_Users]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Users](
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[age] [int] NOT NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
-------------------------------------------------
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Sheet](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[Amount] [int] NOT NULL,
	[TypeOfAmount] [int] NOT NULL,
 CONSTRAINT [PK_Sheet] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Sheet]  WITH CHECK ADD  CONSTRAINT [FK_Sheet_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([ID])
GO

ALTER TABLE [dbo].[Sheet] CHECK CONSTRAINT [FK_Sheet_Users]
GO
------------------------------------------------- Create SP
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Get_count_appartment_by_user_id] @UserId INT
, @CountOfAppartment INT OUTPUT
As

Select @CountOfAppartment = Count(*) From Adress
where UserId = @UserId
GO

CREATE PROCEDURE [dbo].[Get_count_users_by_age] @Age INT
, @CountOfUser INT OUTPUT
As

Select @CountOfUser = Count(*) From Users
where age = @Age
GO

--------------------------------------------- TEST SP
DECLARE @CountOfAppartment int;
EXECUTE dbo.Get_count_appartment_by_user_id 3 , @CountOfAppartment OUTPUT;
PRINT @CountOfAppartment;

declare @CountOfUser int;
exec  Get_count_users_by_age 88, @CountOfUser OUTPUT;
Print @CountOfUser;