﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace Book_ADO_Transaction.Repository
{
    interface IAddressRepository
    {
        void Add(Address address);
        List<Address> GetAll();
        Address GetById(int id);
        void Update(Address address);
        void Delete(int id);
    }
    class AddressRepository : IAddressRepository
    {
        private static string connectionString = @"Data Source = 'DESKTOP-IOHQV8L';
            Initial Catalog='DB_for_ADO';Integrated Security=true;";
        public void Add(Address address)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                var command = connection.CreateCommand();
                var userId = $"'{address.UserId}'";
                if (address.UserId == null)
                {
                    userId = "NULL";
                }
                command.CommandText = @$"INSERT INTO Adress (City, Street, UserId)
                                        VALUES ('{address.City}', '{address.Street}', {userId})
                                        SELECT @@IDENTITY";
                using (var dataReader = command.ExecuteReader())
                {
                    dataReader.Read();
                    var someDec = (decimal)dataReader[0];
                    address.Id = decimal.ToInt32(someDec);
                }
            }
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public List<Address> GetAll()
        {
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                var command = connection.CreateCommand();
                command.CommandText = "Select Id, City, Street, UserId From Adress";
                using (var dataReader = command.ExecuteReader())
                {
                    var listochek = new List<Address>();
                    while (dataReader.Read())
                    {
                        int? userId = null;
                        if (!(dataReader["UserId"] is DBNull))
                        {
                            userId = (int?)dataReader["UserId"];
                        }

                        var adress = new Address
                        {
                            Id = (int)dataReader["Id"],
                            City = (string)dataReader["City"],
                            Street = (string)dataReader["Street"],
                            UserId = userId
                        };
                        listochek.Add(adress);
                    }
                    return listochek;
                }

            }
        }

        public Address GetById(int id)
        {
            throw new NotImplementedException();
        }

        public void Update(Address address)
        {
            throw new NotImplementedException();
        }
    }
    class Address
    {
        public string City { get; set; }
        public string Street { get; set; }
        public int? UserId { get; set; }
        public int Id { get; set; }

    }
}
