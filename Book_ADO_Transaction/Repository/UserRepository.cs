﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Book_ADO_Transaction.Repository
{
    interface IUserRepository
    {
        void Add(User user);
        List<User> GetAll();
        User GetById(int id);
        void Update(User user);
        void Delete(int id);
        void ChangeAddress(int sourceId, int destinationId);
        int GetCountAppartmentByUserId(int userId);
        int GetCountUserByAge(int age);
    }
    class UserRepository : IUserRepository
    {
        private static string connectionString = @"Data Source = 'DESKTOP-IOHQV8L';
            Initial Catalog='DB_for_ADO';Integrated Security=true;";

        public void ChangeAddress(int sourceId, int destinationId)
        {
            // создать транзакцию
            using (var connecting = new SqlConnection(connectionString))// коннектимся к нашей БД, создаём подключение
            {
                SqlTransaction transaction = null;
                try
                {
                    connecting.Open();//открываем подключение
                    transaction = connecting.BeginTransaction();
                    var command1 = connecting.CreateCommand();//получаем Id пользователя, которого надо вписать/выписать
                    command1.Transaction = transaction;
                    command1.CommandText = $@"Select TOP 1 UserId from Adress where ID = {sourceId}";
                    int userId1 = 0;
                    using (var dataReader = command1.ExecuteReader())
                    {
                        dataReader.Read();
                        if (dataReader[0] is DBNull)
                        {
                            throw new ArgumentException($"Source address not have userId.");
                        }
                        userId1 = (int)dataReader[0];
                    }
                    var command2 = connecting.CreateCommand();//проверяем, есть ли кто-то в адресе назначения
                    int userId2 = 0;                //куда мы будем заселять пользователя с нужным Id. если есть - валимся
                    command2.Transaction = transaction;
                    command2.CommandText = $@"Select TOP 1 UserId from Adress where ID = {destinationId}";
                    using (var dataReader = command2.ExecuteReader())
                    {
                        dataReader.Read();
                        if (!(dataReader[0] is DBNull))
                        {
                            userId2 = (int)dataReader[0];
                            throw new ArgumentException($"Destination address already have user: {userId2}.");
                        }
                    }
                    var command3 = connecting.CreateCommand();//заселяем пользователя по новому адресу
                    command3.Transaction = transaction;
                    command3.CommandText = $@"Update Adress SET UserId = {userId1} Where ID = {destinationId}";
                    command3.ExecuteNonQuery();
                    var command4 = connecting.CreateCommand();//выселяем пользователя со старого адреса
                    command4.Transaction = transaction;
                    command4.CommandText = $@"Update Adress set UserId = NULL where ID = {sourceId}";
                    command4.ExecuteNonQuery();

                    transaction.Commit();
                }
                catch (Exception exception)
                {
                    transaction?.Rollback();
                    var oldColor = Console.ForegroundColor;
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"Exception: {exception.Message}");
                    Console.ForegroundColor = oldColor;
                }
            }
        }

        public void Add(User user)
        {//
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                var command = connection.CreateCommand();
                command.CommandText = @$"INSERT INTO Users ([FirstName], [LastName], [age])
                                        VALUES ('{user.FirstName}', '{user.LastName}', {user.Age})
                                        SELECT @@IDENTITY";
                using (var dataReader = command.ExecuteReader())
                {
                    dataReader.Read();
                    user.ID = (int)dataReader[0];
                }
            }
        }

        public void Delete(int id)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                var command = connection.CreateCommand();
                command.CommandText = $"Delete From Users where ID = {id}";
                command.ExecuteNonQuery();
            }
        }

        public List<User> GetAll()
        {
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                var command = connection.CreateCommand();
                command.CommandText = "Select ID, FirstName, LastName, age From Users";
                using (var dataReader = command.ExecuteReader())
                {
                    var listochek = new List<User>();
                    while (dataReader.Read())
                    {
                        var user = new User
                        {
                            ID = (int)dataReader["ID"],
                            FirstName = (string)dataReader["FirstName"],
                            LastName = (string)dataReader["LastName"],
                            Age = (int)dataReader["age"]
                        };
                        listochek.Add(user);
                    }
                    return listochek;
                }

            }
        }


        public User GetById(int id)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                var command = connection.CreateCommand();
                command.CommandText = @$"Select TOP 1 ID, FirstName, LastName, age From Users 
                                            Where ID = {id}";
                using (var dataReader = command.ExecuteReader())
                {
                    var user = new User
                    {
                        ID = (int)dataReader["ID"],
                        FirstName = (string)dataReader["FirstName"],
                        LastName = (string)dataReader["LastName"],
                        Age = (int)dataReader["age"]
                    };
                    return user;
                }
            }
        }

        public void Update(User user)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                var command = connection.CreateCommand();
                command.CommandText = $@"UPDATE Users 
                                        SET [FirstName] = '{user.FirstName}',
                                            [LastName] = '{user.LastName}',
                                            [age] = {user.Age}
                                        Where ID = {user.ID}";
                command.ExecuteNonQuery();
            }
        }

        public int GetCountAppartmentByUserId(int userId)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                var command = connection.CreateCommand();
                command.CommandType = CommandType.StoredProcedure; //указываем тип исполняемой нашей команды - это хранимая процедура
                command.CommandText = "Get_count_appartment_by_user_id"; //указываем имя вызываемой хранимой процедуры
                command.Parameters.AddWithValue("@UserId", userId); //указываем входящий параметр userId
                SqlParameter param = new SqlParameter //объявляем исходящий параметр
                {
                    ParameterName = "@CountOfAppartment", //указываем имя параметра
                    SqlDbType = SqlDbType.Int, // указываем тип параметра
                    Direction = ParameterDirection.Output //указываем направление параметра, что это будет исходящий параметр
                };
                command.Parameters.Add(param); //вставляем исходящий параметр в нашу команду
                command.ExecuteNonQuery(); //выполняем хранимую процедуру
                var result = command.Parameters["@CountOfAppartment"]?.Value; //получаем исходящий параметр, указанный ранее
                if (result == null) 
                {
                    return 0;
                }
                return (int)result;
            }
        }

        public int GetCountUserByAge(int age)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                var command = connection.CreateCommand();
                command.CommandType = CommandType.StoredProcedure; //указываем тип исполняемой нашей команды - это хранимая процедура
                command.CommandText = "Get_count_users_by_age"; //указываем имя вызываемой хранимой процедуры
                command.Parameters.AddWithValue("@Age", age); //указываем входящий параметр age
                SqlParameter param = new SqlParameter //объявляем исходящий параметр
                {
                    ParameterName = "@CountOfUser", //указываем имя параметра
                    SqlDbType = SqlDbType.Int, // указываем тип параметра
                    Direction = ParameterDirection.Output //указываем направление параметра, что это будет исходящий параметр
                };
                command.Parameters.Add(param); //вставляем исходящий параметр в нашу команду
                command.ExecuteNonQuery(); //выполняем хранимую процедуру
                var result = command.Parameters["@CountOfUser"]?.Value; //получаем исходящий параметр, указанный ранее
                if (result == null)
                {
                    return 0;
                }
                return (int)result;
            }
        }
    }
    class User
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public int ID { get; set; }

    }
}
